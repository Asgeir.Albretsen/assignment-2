#include <pthread.h> 
#include <stdio.h>   
#include <stdlib.h>  
#include <unistd.h>  
#include <stdbool.h>
#include <time.h>
#include <semaphore.h>

#define NUM_CUSTOMERS 8
#define NUM_WORKERS 2
#define MAX_SLEEP_TIME 10
#define MAX_QUEUE_LENGTH 3
#define QUEUE_ARRAY_LENGTH 5

void *workerController(void *employee_id);
void workerIdle(int id);
void workerMakeIcecream(int id);

void *customerController(void *personal_id);
void customerIdle(int id);
void customerGetIcecream(int id);

int queue_peek();
int queue_size();
int queue_dequeue();
bool queue_isEmpty();
bool queue_isFull();
bool queue_contains(int data);
void queue_enqueue(int data);

struct Queue {
    int queue[QUEUE_ARRAY_LENGTH];
    int front;
    int rear;
    int itemCount;
};

struct Queue queue;

struct Worker {
    int id;
    bool busy;
    pthread_t thread;
    sem_t semaphore;
};

struct Worker workers[NUM_WORKERS];

struct Customer {
    int id;
    pthread_t thread;
    sem_t semaphore;
};

struct Customer customers[NUM_CUSTOMERS];


int main(int argc, char **argv) {

    // Initialize queue
    queue.front = 0;
    queue.rear = -1;
    queue.itemCount = 0;

    // Initialize worker struct array
    for (int i = 0; i < NUM_WORKERS; i++) {
        workers[i].id = i;
        pthread_create(&workers[i].thread, NULL, workerController, (void *)&workers[i].id);
    }
    // Initialize customer struct array
    for (int i = 0; i < NUM_CUSTOMERS; i++) {
        customers[i].id = i;
        pthread_create(&customers[i].thread, NULL, customerController, (void *)&customers[i].id);
    }

    // Join all threads
    for (int i = 0; i < NUM_WORKERS; i++) {
        pthread_join(workers[i].thread, NULL);
    }
    for (int i = 0; i < NUM_CUSTOMERS; i++) {
        pthread_join(customers[i].thread, NULL);
    }

    return 0;
}

void *workerController(void *employee_id) {
    int id = *(int *)employee_id;

    while (1) {
        workerIdle(id);
        workerMakeIcecream(id);
    }
}

void workerIdle(int id) {
    if (queue_isEmpty()) {
        printf("Worker %d sleeps\n", id);
    }
    sem_wait(&workers[id].semaphore);
}

void workerMakeIcecream(int id) {
    workers[id].busy = true;

    float timeNeededToMakeIcecream = rand() % MAX_SLEEP_TIME + 1;
    int customerID = queue_peek();

    printf("Worker %d makes ice cream for person %d (%f seconds)\n", id, customerID, timeNeededToMakeIcecream);
    queue_dequeue();

    sleep(timeNeededToMakeIcecream);

    printf("Worker %d hands person %d their ice cream\n", id, customerID);
    workers[id].busy = false;

    sem_post(&customers[customerID].semaphore);
}

void *customerController(void *personal_id) {
    int id = *(int *)personal_id;

    while (1) {
        if (!queue_contains(id)) {
            customerIdle(id);
            customerGetIcecream(id);
        }
    }
}

void customerIdle(int id) {
    float timeRelaxing = rand() % MAX_SLEEP_TIME + 1;
    printf("Person %d relaxes at beach (%f seconds)\n", id, timeRelaxing);
    sleep(timeRelaxing);
}

void customerGetIcecream(int id) {
    // If queue is too long, the customer will return to the beach
    if (queue_size() > MAX_QUEUE_LENGTH) {
        printf("Person %d goes to get ice cream, but retuns to the beach as the queue is too long (> %d)\n", id, MAX_QUEUE_LENGTH);
        return;
    }

    printf("Person %d gets in the queue\n", id);
    queue_enqueue(id);

    /*printf("QUEUE\n");
    for (int i = 0; i < QUEUE_ARRAY_LENGTH; i++) {
        printf("%d\n", queue.queue[i]);
    }*/

    // Loop until this customer is at the front of the queue
    while (queue_peek() != id) {}

    // Loop until a worker is available
    while (1) {
        for (int i = 0; i < NUM_WORKERS; i++) {
            if (!workers[i].busy) {
                printf("Person %d asks worker %d for ice cream\n", id, i);
                sem_post(&workers[i].semaphore);
                sem_wait(&customers[id].semaphore);
                return;
            }
        }
    }
}





/*
QUEUE FUNCTIONS UNDER HERE
*/

// Gets the element at the front of the queue
int queue_peek() {
    return queue.queue[queue.front];
}

// Get the current amount of elements in the queue
int queue_size() {
    return queue.itemCount;
}  

// Remove the element at the front of the queue
int queue_dequeue() {
    int data = queue.queue[queue.front++];
	
    if(queue.front == QUEUE_ARRAY_LENGTH) {
        queue.front = 0;
    }
	
    queue.itemCount--;
    return data;  
}

// Checks if the queue is empty
bool queue_isEmpty() {
    return queue.itemCount == 0;
}

// Checks if the queue is full
bool queue_isFull() {
    return queue.itemCount == QUEUE_ARRAY_LENGTH;
}

// Checks if the data parameter is in the queue
bool queue_contains(int data) {
    if (queue_size() == 0) { 
        return false;
    }

    int index = queue.front;
    for (int i = 0; i < QUEUE_ARRAY_LENGTH; i++) {
        // Return true if parameter is found in the queue
        if (queue.queue[index] == data) {
            return true;
        }

        // Return false if we reach the end before finding the parameter
        if (index == queue.rear) {
            return false;
        }

        // Loop around the array representation of the queue if we get to the end of the array, otherwise add 1 to the index
        if (index == QUEUE_ARRAY_LENGTH) {
            index = 0;
        } else {
            index++;
        }
    }
    return false;
}

// Place an element at the back of the queue
void queue_enqueue(int data) {

    // Only insert if queue is not full
    if(!queue_isFull()) {
        if(queue.rear == QUEUE_ARRAY_LENGTH-1) {
            queue.rear = -1;            
        }       

        queue.queue[++queue.rear] = data;
        queue.itemCount++;
    }
}